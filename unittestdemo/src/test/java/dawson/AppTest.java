package dawson;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    
    @Test
    public void shouldAnswerWith3() {
        assertEquals("Test to see if it echos same input", 5, App.echo(5));
    }
    
    @Test
    public void shouldAnswerWith5() {
        assertEquals("Returns a value one more than x", 6, App.oneMore(5));
    }
}
